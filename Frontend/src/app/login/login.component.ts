import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  customer:any;

  constructor(private service : CustomerService, private toastr: ToastrService, private router: Router){}

  async loginSubmit(loginForm:any){

    if(loginForm.emailId == 'admin' && loginForm.password == 'admin'){
      
    }else{
      await this.service.customerLogin(loginForm.emailId, loginForm.password).then((data:any) =>{
          this.customer = data;
        });
      
      if(this.customer != null){
        this.toastr.success(
          "Login Successfully", 
          'Login Status', 
          { timeOut:3000, progressBar : true, progressAnimation : 'increasing'
        });
        this.service.isCustomerLoggedIn();
        this.router.navigate(['']);
      }else{
        this.toastr.error("Invalid Credentials", 'Login Status', { timeOut:5000, progressBar: true, progressAnimation:'increasing'});
      }
      
    }
  }
}
